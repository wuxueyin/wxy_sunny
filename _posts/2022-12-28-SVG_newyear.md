---
title: SVG之动弹的Happy New Year
excerpt_separator: "<!--more-->"
categories: 
  - svg制作
tags:
  - svg
---
#### New year is coming......

<!--more-->
Happy New Year!
 <style>
 @import url("https://fonts.googleapis.com/css2?family=Recursive:slnt,wght,CASL,CRSV,MONO@-15..0,300..800,0..1,0..1,0..1&display=swap");

body {
  height: 100vh;
  display: grid;
  place-items: center;
  background-color: #3974A8;
}

.text {
  font-family: "Recursive", monospace;
  font-size: clamp(4em, 15vmin, 20em);
  text-shadow: 0;
  color: #fefae0;
  animation: anime 2s cubic-bezier(0.445, 0.05, 0.55, 0.95) alternate infinite;
}

@keyframes anime {
  from {
    font-variation-settings: "wght" 800, "slnt" 0;
    text-shadow: -5px 10px 0px #00e6e6, -10px 20px 0px #01cccc,
      -15px 30px 0px #00bdbd;
  }
  to {
    font-variation-settings: "wght" 300, "slnt" 15;
    text-shadow: none;
  }
}
</style>

<div class="text">Happy<br>New Year!</div>