---
layout: page
title: 给文字增加阴影 让文字效果变不一样的感觉
excerpt_separator: "<!--more-->"
categories:
     - 平面设计
---

#### shadow of words
<!--more-->
+ text-shadow 属性 可参考[菜鸟教程](https://www.runoob.com/cssref/css3-pr-text-shadow.html)

{ text-shadow: 2px 2px #ff0000; }

+ 第一个值：阴影的右侧偏移量（x轴）
+ 第二个值:阴影的下方偏移量（y轴）
+ 第三个值模糊半径:阴影的影响范围，不能为负值，值越大越模糊，第四个值则是色值