---
layout: page
title: 我们学习的html5
excerpt_separator: "<!--more-->"
categories:
     - 网站设计
---

#### HTML
<!--more-->
* 超文本标记语言（英语：HyperText Markup Language，简称: [HTML](https://www.runoob.com/html/html-tutorial.html)）是一种用于创建网页的标准标记语言。可以使用 HTML 来建立自己的 WEB 站点，HTML 运行在浏览器上，由浏览器来解析。


### 解析
 + <!DOCTYPE html> 声明为 HTML5 文档
 + <html> 元素是 HTML 页面的根元素
 + <head> 元素包含了文档的元（meta）数据，如 <meta charset="utf-8"> 定义网页编码格式为 utf-8。
 + <title> 元素描述了文档的标题
 + <body> 元素包含了可见的页面内容
 + h1 元素定义一个大标题
 + <p> 元素定义一个段落

### 什么是HTML?
+ HTML 是用来描述网页的一种语言。
+ HTML 指的是超文本标记语言: HyperText Markup Language
+ HTML 不是一种编程语言，而是一种标记语言,标记语言是一套标记标签 (markup tag)
+ HTML 使用标记标签来描述网页
+ HTML 文档包含了HTML 标签及文本内容
+ HTML文档也叫做 web 页面

### HTML 标签
+ HTML 标记标签通常被称为 HTML 标签 (HTML tag)。
+ HTML 标签是由尖括号包围的关键词，比如 <html>
+ HTML 标签通常是成对出现的，比如 <b> 和 </b>
+ 标签对中的第一个标签是开始标签，第二个标签是结束标签
+ 开始和结束标签也被称为开放标签和闭合标签
+ <标签>内容</标签>